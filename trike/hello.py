def greet(name):
    return f'Hello, {name}. Covfefe!'

if __name__ == '__main__':
    x = greet("test")
    assert x == 'Hello, test'
    print("test successful")